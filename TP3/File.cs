﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TP3
{
    public class File
    {
        private String filePath;
        private long fileSize;
        private String format;
        private String fileName;

        [XmlAttribute("filepath")]
        public String FilePath { get => filePath; set => filePath = value; }
        [XmlAttribute("filesize")]
        public long FileSize { get => fileSize; set => fileSize =value; }
        [XmlAttribute("extension")]
        public String Format { get => format; set => format = value; }
        [XmlElement("filename")]
        public String FileName { get => fileName; set => fileName = value; }

        public File(String path, long size, String form, String name)
        {
            this.filePath = path;
            this.fileSize = size;
            this.format = form;
            this.fileName = name;
        }
        public File()
        {

        }
    }
}
