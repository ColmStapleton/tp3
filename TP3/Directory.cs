﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TP3
{
    [XmlRoot("Folder")]
    public class Directory
    {

        private String folderName;
        private int nbSubFolders;
        private int nbFiles;
        private String folderPath;
        private List<Directory> subDirectories;
        private List<File> files;

        [XmlAttribute("folder_name")]
        public String FolderName { get => folderName; set => folderName = value; }
        [XmlAttribute("nb_subfolders")]
        public int NbSubFolders { get => nbSubFolders; set => nbSubFolders = value; }
        [XmlAttribute("nb_files")]
        public int NbFiles { get => nbFiles; set => nbFiles = value; }
        [XmlArray("Subfolders")]
        [XmlArrayItem("Subfolder")]
        public List<Directory> SubDirectories { get => subDirectories; set => subDirectories = value; }
        [XmlArray("files")]
        [XmlArrayItem("File")]
        public List<File> Files { get => files; set => files = value; }
        [XmlElement("folder_path")]
        public String FolderPath { get => folderPath; set => folderPath = value; }


        public Directory(String name, int subFolders, int nbFiles, String folderPath, List<Directory> subDirs, List<File> files)
        {
            this.folderName = name;
            this.nbSubFolders = subFolders;
            this.nbFiles = nbFiles;
            this.folderPath = folderPath;
            this.subDirectories = subDirs;
            this.files = files;
        }

        public Directory()
        {

        }

    }
}
