﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TP3
{
    class Program
    {
        static void Main(string[] args)
        {

            //Choix du répertoire à parcourir
            DirectoryInfo home = new DirectoryInfo(@"E:\Movies");
            Directory dir = new Directory();

            if (home.Exists)
            {
                //On lance la fonction FetchDir pour parcourir le répertoire 'home'
                dir = FetchDir(home);
            }

            //Sérialisation d'un objet de type Directory
            XmlSerializer xs = new XmlSerializer(typeof(Directory));
            using (StreamWriter wr = new StreamWriter("directory.xml"))
            {
                xs.Serialize(wr, dir);
            }
        }

        static Directory FetchDir(DirectoryInfo dir)
        { 
            //Déclaration des listes de type File et de type Directory
            List<File> myFiles = new List<File>();
            List<Directory> myDirs = new List<Directory>();

            //On récupère les fichiers dans le répertoire en cours
            foreach (System.IO.FileInfo files in dir.GetFiles())
            {
                //Création d'une nouvelle instance de File où on conserve les données pour chaque fichier du répertoire
                File f = new File(files.FullName, files.Length, files.Extension, files.Name);

                //Ajout de l'objet créé à la liste myFiles
                myFiles.Add(f);
            }

            //ON récupère tous les répertoires fils du répertoire en cours
            foreach (DirectoryInfo elt in dir.GetDirectories())
            {
                if (elt.Exists)
                {
                    //On lance la fonction FetchDir sur chaque répertoire fils, qui nous retourne un objet de type Directory
                    Directory thisDir = FetchDir(elt);

                    //Ajout l'objet créé à la liste de type Directory
                    myDirs.Add(thisDir);
                }
            }

            //Création d'une instance de Directory où on conserve les données appropriées
            Directory d = new Directory(dir.Name, myDirs.Count, myFiles.Count, dir.FullName, myDirs, myFiles);

            //Retour et fin
            return d;
       }
    }
}
